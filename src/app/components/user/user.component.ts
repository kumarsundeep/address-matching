import { Component, OnInit } from '@angular/core';
//import {Pipe, PipeTransform} from '@angular/core';
//Imported, For collecting data in organised manner - Will done after successfull address match and listing
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  name:string;
  email:string;
  addresses:string[];
  filteredItems:string[];
  hello:any;

  constructor(private dataService:DataService) {
    //console.log('constructor ran..');

  }

  ngOnInit() {
    //console.log('ngOnInit ran...');

    this.name = 'User Address';
    //Some hardcoded addresses
    this.addresses = ['6480, Sector C6, Vasant Kunj', '8231, Sector C8, Vasant Kunj', 'C-6/62680, Vasant Kunj'];

  }



  addAddress(address){
    console.log(address);
    this.addresses.unshift(address);
    return false;
  }

  deleteAddress(i){
    this.addresses.splice(i, 1);
  }



  //Filtering Addresses

  assignCopy(){
    this.filteredItems = Object.assign([], this.addresses);
  }
  filterItem(value){
    if(!value) this.assignCopy(); //when nothing has typed
    this.filteredItems = Object.assign([], this.addresses).filter(
      item => item.addressName.indexOf(value) > -1
    )
  }

}


/*
@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {
  public transform(value, keys: string, term: string) {

    if (!term) return value;
    return (value || []).filter((address) => keys.split(',').some(key => address.hasOwnProperty(key) && new RegExp(term, 'gi').test(address[key])));

  }
}*/
